# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

LANCER LE PROJET AVEC DOCKER

Lancer un terminal à la racine du projet simple-http-hello-world.

Lancer la commande suivante : docker build -t simple-hello .

Enfin lancer la commande suivante : docker run -it -d -p 8182:8181 simple-hello

L'application est maintenant accessible à l'adresse suivante : localhost:8182/hello